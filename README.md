# README #

### What is this repository for? ###

This repository contains sample code used for the "Benchmarking Clustering Algorithms" a thesis completed in 2014 for the Bachelor of Engineering (Software Engineering) at the University of Queensland. It will also contain the PDF of this thesis once the assessment has been completed.
### Whats in the repository? ###

The code in sample_code.r contains classes which extend the clusterSim library and replicate the Methodology described in the thesis. The main requirement is the clusterSim package and its dependencies.